package com.data.api.game.destiny.dao;

import com.data.api.game.destiny.model.DestinyActivityModifierDefinition;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityModifierRepository extends CrudRepository<DestinyActivityModifierDefinition, Long> {
}
