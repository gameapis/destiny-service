package com.data.api.game.destiny.dao;

import com.data.api.game.destiny.model.DestinyMilestoneDefinition;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MilestoneRepository extends CrudRepository<DestinyMilestoneDefinition, Long> {

}
