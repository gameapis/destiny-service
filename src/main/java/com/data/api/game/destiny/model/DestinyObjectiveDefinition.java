package com.data.api.game.destiny.model;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DestinyObjectiveDefinition")
@Getter
public class DestinyObjectiveDefinition {

    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "json")
    private String json;

    protected DestinyObjectiveDefinition() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }
}

