package com.data.api.game.destiny;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class EvictScheduler {

    Logger logger = LoggerFactory.getLogger(EvictScheduler.class);

    @Scheduled(cron = "0 30 13 * * Tue")
    @CacheEvict(cacheNames = "milestones", allEntries = true)
    public void milestoneCacheEvict() {
        logger.info("Milestone Cache has been Emptied");
    }
}

