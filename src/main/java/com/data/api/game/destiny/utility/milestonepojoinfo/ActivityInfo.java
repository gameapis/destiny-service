package com.data.api.game.destiny.utility.milestonepojoinfo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ActivityInfo {

    private String activityName;
    private String activityIcon;
    private List<ObjectiveInfo> objectiveInfoList;
    private List<ModifierInfo> modifierInfoList;
}
