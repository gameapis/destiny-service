package com.data.api.game.destiny.model;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DestinyActivityModifierDefinition")
@Getter
public class DestinyActivityModifierDefinition {

    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "json")
    private String json;

    protected DestinyActivityModifierDefinition() {
    }

    public long getId() {
        return id;
    }

    public String getJson() {
        return json;
    }
}
