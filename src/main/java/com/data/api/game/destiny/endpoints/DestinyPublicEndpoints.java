package com.data.api.game.destiny.endpoints;

import com.data.api.game.destiny.pojo.Milestones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class DestinyPublicEndpoints {

    @Autowired
    @Qualifier("destiny")
    private RestTemplate restCalls;

    @Autowired
    @Qualifier("destiny")
    private HttpEntity<String> httpEntity;

    public Milestones getMilestones() {

        return restCalls.exchange("https://www.bungie.net/Platform/Destiny2/Milestones/",
                HttpMethod.GET, httpEntity, Milestones.class).getBody();
    }
}
