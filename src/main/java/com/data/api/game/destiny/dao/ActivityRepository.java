package com.data.api.game.destiny.dao;

import com.data.api.game.destiny.model.DestinyActivityDefinition;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityRepository extends CrudRepository<DestinyActivityDefinition, Long> {

}
