package com.data.api.game.destiny.model;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DestinyMilestoneDefinition")
@Getter
public class DestinyMilestoneDefinition {

    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "json")
    private String json;

    protected DestinyMilestoneDefinition() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }


}

