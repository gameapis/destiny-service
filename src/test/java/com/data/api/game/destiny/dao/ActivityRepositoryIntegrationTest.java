package com.data.api.game.destiny.dao;

import com.data.api.game.destiny.model.DestinyActivityDefinition;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class ActivityRepositoryIntegrationTest {

    @Autowired
    private ActivityRepository activityRepository;

    @Test
    public void whenFindById_ReturnActivity(){
        DestinyActivityDefinition activity = activityRepository.findById(1428050875L).get();

        assertThat(activity).isNotNull();
        assertThat(activity.getId()).isEqualTo(1428050875L);
        assertThat(activity.getJson()).isNotBlank();
    }
}
