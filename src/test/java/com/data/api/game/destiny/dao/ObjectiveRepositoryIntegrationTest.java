package com.data.api.game.destiny.dao;

import com.data.api.game.destiny.model.DestinyObjectiveDefinition;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class ObjectiveRepositoryIntegrationTest {

    @Autowired
    private ObjectiveRepository objectiveRepository;

    private DestinyObjectiveDefinition objective;

    @Test
    public void whenFindById_ReturnObjective(){

        objective = objectiveRepository.findById(562619790L).get();

        assertThat(objective).isNotNull();
        assertThat(objective.getId()).isEqualTo(562619790L);
        assertThat(objective.getJson()).isNotBlank();
    }
}
