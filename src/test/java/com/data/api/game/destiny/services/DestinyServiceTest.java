package com.data.api.game.destiny.services;

import com.data.api.game.destiny.service.milestoneService;
import com.data.api.game.destiny.utility.milestonepojoinfo.MilestoneInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class DestinyServiceTest {

    private milestoneService milestoneService;

    private List<MilestoneInfo> milestoneInfos;


    public void getMilestoneIds_and_ReturnMilestoneInfo() throws IOException {

        milestoneInfos = new ArrayList<MilestoneInfo>();

        milestoneInfos = milestoneService.getMilestoneInfo();

        assertThat(milestoneInfos).isNotNull().isNotEmpty();
    }
}
