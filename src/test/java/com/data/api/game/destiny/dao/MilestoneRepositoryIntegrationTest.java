package com.data.api.game.destiny.dao;

import com.data.api.game.destiny.model.DestinyMilestoneDefinition;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class MilestoneRepositoryIntegrationTest {

    @Autowired
    private MilestoneRepository milestoneRepository;

    private DestinyMilestoneDefinition milestone;

    @Test
    public void whenFindById_ReturnMilestone(){

        milestone = milestoneRepository.findById(-41829105L).get();

        assertThat(milestone).isNotNull();
        assertThat(milestone.getId()).isEqualTo(-41829105L);
        assertThat(milestone.getJson()).isNotBlank();
    }
}
