package com.data.api.game.destiny.endpoints;

import com.data.api.game.destiny.pojo.Milestones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DestinyPublicEndpointsIntegrationTest {

    @Autowired
    private DestinyPublicEndpoints destinyPublicEndpoints;

    @Test
    public void whenCallingApi_thenReturnMilestones(){

        Milestones milestones = destinyPublicEndpoints.getMilestones();

        assertThat(milestones).isNotNull();
    }
}
