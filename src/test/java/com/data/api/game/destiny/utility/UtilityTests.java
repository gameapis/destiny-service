package com.data.api.game.destiny.utility;

import com.data.api.game.destiny.endpoints.DestinyPublicEndpoints;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UtilityTests {

    @Autowired
    private DestinyPublicEndpoints destinyPublicEndpoints;

    @Autowired
    private MilestoneUtility milestoneUtility;

    @Test
    public void getMilestoneHashes_ReturnThemAsIds() {

        List<MilestoneUtility.MilestoneId> milestoneIds = milestoneUtility.getMilestoneIds(destinyPublicEndpoints.getMilestones().getMap());

        for (MilestoneUtility.MilestoneId id : milestoneIds) {

            assertThat(id.getMilestoneId()).isNotNull();
        }



    }

}
